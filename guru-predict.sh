#!/usr/bin/env bash
INPUT_ARGS="$@"
docker run -u $(id -u):$(id -g) --rm -v $(pwd):/data -w /data pavelsr/guru-core bash -c "python /app/predict.py -f /data/orders.csv ${INPUT_ARGS}"
# add -it options if needed
