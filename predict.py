#!/usr/bin/env python

import sys
import click
import pandas as pd
import numpy as np
import json

from neuro import predict
from df_utils import filter_df, get_extended_orders_df

from pprint import pprint

@click.command()
# @click.argument('days')
@click.option('--file', '-f',  default='orders.csv', show_default=True, help='Location of dataset csv file', type=click.Path(exists=True))
@click.option('--days', '-d',  type=int, default=28, show_default=True, help='Amount of days in forecast')
@click.option('--iwidth', '-i', type=int, default=28, show_default=True, help='How many days of history to take into consideration (input_width of WindowGenerator)',)
@click.option('--units', '-u', type=int, default=32, show_default=True, help='Positive integer, dimensionality of the output space at tf.keras.layers.LSTM')
@click.option('--toint', default=False, show_default=True, help='Make data integer', is_flag=True)

@click.option('--city', type=str, show_default=True, help='Predict orders only for this city')
@click.option('--district', type=str, show_default=True, help='Predict orders only for this district (works only if --city option is set)')
@click.option('--service', type=str, show_default=True, help='Predict orders only for this service')

# --service Укладка плитки --district Южный административный округ --city Москва --date 2021-05-01


# @click.option('--startDate',  default='2020-01-01', show_default=True, help='Day to start analysis from, ISO 8601 format' )

def main(file, days, iwidth, units, toint, city, district, service):
    df = pd.read_csv( file, header=0, delimiter=',')

    filter_params = { "city":city,"district":district,"service":service }

    try:
        df = filter_df(df,filter_params)
    except Exception as e:
        return click.echo( json.dumps(e.args[0]) )

    df = get_extended_orders_df(df)

    # column_indices = {name: i for i, name in enumerate(df.columns)}
    predictions, train_mean, train_std = predict(df,days,iwidth,units)

    # pprint(history.history['mean_squared_error'])
    # pprint(history.history['mean_absolute_percentage_error'])
    # pprint(history.history['val_loss'])

    if toint:
        predictions = [int(a) for a in predictions]
    else:
        predictions = np.float64(predictions).tolist()

    return click.echo( json.dumps({ "predictions":predictions, "train_mean": train_mean.orders_n, "train_std": train_std.orders_n }))

if __name__ == "__main__":
    main()
