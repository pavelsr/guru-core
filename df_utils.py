import pandas as pd
import numpy as np
import json
from pprint import pprint

def defSeason(month):
    if (month in [12,1,2]):
        return 0
    elif (month in [3,4,5]):
        return 1
    elif (month in [6,7,8]):
        return 2
    elif (month in [9,10,11]):
        return 3
    else:
        return None

def defIsHoliday(day_of_week):
    if (day_of_week in [5,6]):
        return 1
    else:
        return 0

def defAvgT(date, df_src):
    return df.loc[df['date'] == date,['date', 'weather','is_rain']].mean()

# city, district, service
def filter_df(df_src, params):
    df = df_src
    if ('city' in params and params['city']):
        df = df.loc[df['city'] == params['city']]
        if not df.size:
            valid_prm = df_src['city'].unique().tolist()
            raise Exception({'error':'bad city param - no orders with such city in dataset, so no history for prediction', 'available_cities': valid_prm })
        if ('district' in params and params['district']):
            valid_prm = df['district'].unique().tolist()
            df = df.loc[df['district'] == params['district']]
            if not df.size:
                raise Exception({'error':'bad district param - no orders with such city+district in dataset, so no history for prediction', 'city': params['city'], 'available_districts': valid_prm })
            if ('service' in params and params['service']):
                valid_prm = df['service'].unique().tolist()
                df = df.loc[df['service'] == params['service']]
                if not df.size:
                    raise Exception({'error':'bad service param - no orders with such city+district+service in dataset, so no history for prediction', 'city': params['city'], 'district': params['district'], 'available_services': valid_prm })

    if ('service' in params and params['service']):
        valid_prm = df['service'].unique().tolist()
        df = df.loc[df['service'] == params['service']]
        if not df.size:
            raise Exception({'error':'bad service param - no orders with such service in dataset, so no history for prediction', 'available_services': valid_prm })

    if not df.size:
        raise Exception('dataset is empty, nothing to analyse')

    return df

def get_extended_orders_df(df_src):
    startdate = '2020-01-01'
    date_stat = df_src['date'].value_counts()
    date_stat_df = pd.DataFrame({'date': date_stat.index,'orders_n':date_stat.values})
    date_stat_df['date'] = pd.to_datetime(date_stat_df['date'])
    date_stat_df = date_stat_df.set_index(pd.DatetimeIndex(date_stat_df['date']))
    dti = pd.date_range(startdate, periods=365, freq="D")
    date_stat_df = date_stat_df.reindex(dti)
    date_stat_df['date'].fillna( date_stat_df.index.to_series(), inplace=True)
    date_stat_df['orders_n'] = date_stat_df['orders_n'].fillna(0).astype(np.int64)
    date_stat_df = date_stat_df.drop(columns=['date'])
    return date_stat_df
