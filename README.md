# Что это?

Python ядро проекта

# Как запустить?

## Docker

```bash
$ docker run -it --rm -v $(pwd):/app -w /app pavelsr/guru-core python predict.py --help
```

## Natively

(maybe useful for development)

```
$ git clone git@gitlab.com:pavelsr/guru-core.git
$ python -m pip install -r requirements.txt
$ predict.py --help
```

Возможно может потребоваться установка пакета `gcc-c++`

## Bash CLI wrapper under docker

(production-ready, supress tf warnings)

монтирует текущую директорию в папку /data docker контейнера

```bash
# place orders.csv at this folder
$ sudo curl -L https://gitlab.com/pavelsr/guru-core/-/raw/master/guru-predict.sh -o /usr/local/bin/guru-predict && sudo chmod +x /usr/local/bin/guru-predict
$ guru-predict
```

# Примеры команды к predict.py или guru-predict

```
$ python predict.py --city Москва

```
