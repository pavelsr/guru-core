# docker build -t pavelsr/guru-core .
# docker push pavelsr/guru-core

FROM tensorflow/tensorflow

WORKDIR /app
COPY requirements.txt /app/
RUN python -m pip install -r requirements.txt
RUN rm requirements.txt
COPY predict.py neuro.py df_utils.py user_data.py orders.csv /app/
RUN mkdir /data
