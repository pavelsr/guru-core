#!/usr/bin/env python

import click
import pandas as pd
import numpy as np
import json

df = pd.read_csv('orders.csv',header=0, delimiter=';')

def get_user_ltv(df, user_id):
    return np.float64( df.loc[df['user_id'] == user_id].loc[df['is_completed'] == 'Выполнен']['price2'].sum() )


@click.command()
@click.option('--file', '-f',  default='orders.csv', show_default=True, help='Location of dataset csv file', type=click.Path(exists=True))
@click.option('--user_id', '-u', type=int, help='user_id, e.g. 67888')

def main(file, user_id):
    df = pd.read_csv( file, header=0, delimiter=',')
    result = { "ltv": get_user_ltv(df, user_id) }
    return click.echo( json.dumps(result) )

if __name__ == "__main__":
    main()
